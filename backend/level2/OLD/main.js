var http = require('http');
var server = http.createServer(function(req, res) {
	var fs = require('fs');

	//Afficher le contenu du fichier
	var fileContent = fs.readFileSync('input.json','utf-8');
	let fileContentBis = JSON.parse(fileContent);

//Création de tableaux
	var idCar = fileContentBis.cars[0].id
	var priceCarDay = fileContentBis.cars[0].price_per_day
	var priceCarkm = fileContentBis.cars[0].price_per_km
	var idRental = [];
	var idRentalCar = [];
	var dateDebRental = [];
	var dateEndRental = [];
	var distanceRental = [];
	var distanceRental = [];
	var dix = 1-0.1;
	var trente = 1-0.3;
	var cinquante = 1-0.5;

//Parcourir les éléments et les insérer dans les tableaux
	for(var k in fileContentBis.rentals) {
     	idRental.push(fileContentBis.rentals[k].id);
     	idRentalCar.push(fileContentBis.rentals[k].car_id);
     	dateDebRental.push(fileContentBis.rentals[k].start_date);
     	dateEndRental.push(fileContentBis.rentals[k].end_date);
     	distanceRental.push(fileContentBis.rentals[k].distance);  		
	}

//Premier Calcul
	if(idRentalCar[0]){
 		var date1 = new Date(dateDebRental[0]);
		var date2 = new Date(dateEndRental[0]);
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays1 = Math.ceil((timeDiff+1) / (1000 * 3600 * 24));
		price1 = (diffDays1 * priceCarDay) + (distanceRental[0] * priceCarkm);
	}

//Second calcul
	if(idRentalCar[0]){
 		var date1 = new Date(dateDebRental[1]);
		var date2 = new Date(dateEndRental[1]);
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays1 = Math.ceil((timeDiff+1) / (1000 * 3600 * 24));
		price2 = (((diffDays1-1) * priceCarDay) +((diffDays1-1) * priceCarDay)*dix)  + (distanceRental[1] * priceCarkm);

	}

//Troisième Calcul
	if(idRentalCar[0]){
 		var date1 = new Date(dateDebRental[2]);
		var date2 = new Date(dateEndRental[2]);
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays1 = Math.ceil((timeDiff+1) / (1000 * 3600 * 24));
		price3 = (((diffDays1-11) * priceCarDay)+(((diffDays1-9) * priceCarDay)*dix)+(((diffDays1-6) * priceCarDay)*trente)+(((diffDays1-10) * priceCarDay)*cinquante))+ (distanceRental[2] * priceCarkm);

	}

//écrire les données dans un JSON


	var obj = {
  		 Rentals: []
	};
	var json = obj.Rentals.push(
		{
			id: 1,
			price: price1
		},
		{
			id: 2, 
			price: price2
		},
		{
			id: 3, 
			price: price3
		});
	var json = JSON.stringify(obj,null,2);
    fs.writeFileSync('OutPutCarRental2.json', json)

  
});
server.listen(8080);