//Afficher le contenu du fichier
const fileClassCar = require('/jobs/backend/level3/rentalClass.js');
let Cars = new fileClassCar(this.fileContent);
let fileContent = Cars.fileContent;
const fs = require('fs');
let obj = {
    Rentals: []
};

fileContent.rentals.map(rentals => { 
let price = Cars.getCar(rentals.car_id,rentals.start_date,rentals.end_date,rentals.distance);

	obj.Rentals.push(
		{
			id:rentals.id, 
			price:price.priceCalc,
			commission: {
        	insurance_fee: price.insuranceFee,
      		assistance_fee: price.assistanceFee,
        	cliclic_fee: price.cliclicFee
        	}
		}
	);
});
let data = JSON.stringify(obj,null,2);
fs.writeFileSync('/jobs/backend/level3/data/OutPutCarRental3.json',data);

