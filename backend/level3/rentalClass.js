
class cars {
	constructor(fileContent){
		this.fileContent = require('/jobs/backend/level3/data/input.json');
	}
	getCar(car_id,start_date, end_date,distance) {
    	let price = {
    		priceCalc: {},
    		insuranceFee: {},
   			assistanceFee: {},
   			cliclicFee: {}
	};
	this.fileContent.cars.map(car => { 	
		if(car.id == car_id){
			let date1 = new Date(start_date);
			let date2 = new Date(end_date);
			let timeDiff = Math.abs(date2.getTime() - date1.getTime());
			let diffDays1 = Math.ceil((timeDiff + 1) / (1000 * 3600 * 24));
			let commission;
			if(diffDays1 == 1) {
				price.priceCalc = (diffDays1 * car.price_per_day) + (distance * car.price_per_km);
				commission = (price.priceCalc - (price.priceCalc * 0.7));
				price.insuranceFee = commission / 2;
				price.assistanceFee = diffDays1 * 100;
				price.cliclicFee = commission - price.insuranceFee - price.assistanceFee;
			} else if(diffDays1 <= 3) {
				price.priceCalc = ((diffDays1 - 1) * car.price_per_day) + (((diffDays1 - 1) * car.price_per_day) * 0.9) + 
				(distance * car.price_per_km);
				commission = (price.priceCalc - (price.priceCalc * 0.7));
				price.insuranceFee = commission / 2;
				price.assistanceFee = diffDays1 * 100;
				price.cliclicFee = commission - price.insuranceFee - price.assistanceFee;
			} else if(diffDays1 >= 4) {
				price.priceCalc = ((diffDays1 - 11) * car.price_per_day) + (((diffDays1 - 9) * car.price_per_day) * 0.9) +
				(((diffDays1 - 6) * car.price_per_day) * 0.7) + (((diffDays1 - 10) * car.price_per_day) * 0.5) + 
				(distance * car.price_per_km);
				commission = (price.priceCalc - (price.priceCalc * 0.7));
				price.insuranceFee = commission / 2;
				price.assistanceFee = diffDays1 * 100;
				price.cliclicFee = commission - price.insuranceFee - price.assistanceFee;
			}
		}
	  });
    return price;
	}
}

module.exports = cars;
 
