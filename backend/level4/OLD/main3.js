//Afficher le contenu du fichier
const fileClassCar = require('/jobs/backend/level4/rentalClass.js');
let Cars = new fileClassCar(this.fileContent);
let fileContent = Cars.fileContent;
const fs = require('fs');
let obj = {
    Rentals: []
};

fileContent.rentals.map(rentals => { 
let price = Cars.getCar(rentals.car_id,rentals.start_date,rentals.end_date,rentals.distance);

	obj.Rentals.push(
		{
			id:rentals.id, 
		    actions: [
	        {
	          who: "driver",
	          type: "debit",
	          amount: price.priceCalc
	        },
	        {
	          who: "owner",
	          type: "credit",
	          amount: price.owner
	        },
	        {
	          who: "insurance",
	          type: "credit",
	          amount: price.insuranceFee
	        },
	        {
	          who: "assistance",
	          type: "credit",
	          amount: price.assistanceFee
	        },
	        {
	          who: "cliclic",
	          type: "credit",
	          amount: price.cliclicFee
	        }
	      	]
		}
	);
});
let data = JSON.stringify(obj,null,2);
fs.writeFileSync('/jobs/backend/level4/data/OutPutCarRental4.json',data);

