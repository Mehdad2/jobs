//Afficher le contenu du fichier
const fileContent = require('./input.json');
//let fileContentBis = JSON.parse(fileContent);
const fs = require('fs');
let obj = {
    Rentals: []
};
let json;
fileContent.rentals.map(rentals => { 
let price = getCar(rentals.car_id,rentals.start_date,rentals.end_date,rentals.distance);
	obj.Rentals.push(
		{
			id:rentals.id, 
			price:price
		}
	);
});
let data = JSON.stringify(obj,null,2);
fs.writeFileSync('./jobs/backend/CLICLIC-TEST/level1/OutPutCarRental2.json',data);
function getCar(car_id,start_date, end_date,distance) {
    let price;
	fileContent.cars.map(car => { 	
			if(car.id == car_id){
				let date1 = new Date(start_date);
				let date2 = new Date(end_date);
				let timeDiff = Math.abs(date2.getTime() - date1.getTime());
				let diffDays1 = Math.ceil((timeDiff+1) / (1000 * 3600 * 24));
				return price = (diffDays1 * car.price_per_day) + (distance * car.price_per_km);
			}
    });
    return price;
}