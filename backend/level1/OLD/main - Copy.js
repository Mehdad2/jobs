var http = require('http');
var server = http.createServer(function(req, res) {
	 
	var fs = require('fs');

	//Afficher le contenu du fichier
	var fileContent = fs.readFileSync('input.json','utf-8');
	let fileContentBis = JSON.parse(fileContent);

//Création de tableaux
	var idCar = [];
	var priceCarDay = [];
	var priceCarkm = [];
	var idRental = [];
	var idRentalCar = [];
	var dateDebRental = [];
	var dateEndRental = [];
	var distanceRental = [];
	var distanceRental = [];

//Parcourir les éléments et les insérer dans les tableaux
	for(var k in fileContentBis.cars ||  fileContentBis.rentals) {
	 	idCar.push(fileContentBis.cars[k].id);
	 	priceCarDay.push(fileContentBis.cars[k].price_per_day);
     	priceCarkm.push(fileContentBis.cars[k].price_per_km);
     	idRental.push(fileContentBis.rentals[k].id);
     	idRentalCar.push(fileContentBis.rentals[k].car_id);
     	dateDebRental.push(fileContentBis.rentals[k].start_date);
     	dateEndRental.push(fileContentBis.rentals[k].end_date);
     	distanceRental.push(fileContentBis.rentals[k].distance); 
   	
	}

//fonction de calcul des prix
	function pricex(a,b,c,d,e){
		var date1 = new Date(dateDebRental[a]);
		var date2 = new Date(dateEndRental[b]);
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays1 = Math.ceil((timeDiff+1) / (1000 * 3600 * 24));
	 	price = (diffDays1 * priceCarDay[c]) + (distanceRental[d] * priceCarkm[e]);
		return price;		
	}

	var price1 = pricex(0,0,0,0,0);
	var price2 = pricex(1,1,0,1,0);
	var price3 = pricex(2,2,1,2,1);

//écrire les données dans un JSON
	var obj = {
  		 Rentals: []
	};
	var json = obj.Rentals.push(
		{
			id: 1, 
			price: price1
		},
		{
			id: 2, 
			price: price2
		},
		{
			id: 3,
			price: price3
		}
		);
	var json = JSON.stringify(obj,null,2);
    fs.writeFileSync('OutPutCarRental.json', json)


});
server.listen(8080);