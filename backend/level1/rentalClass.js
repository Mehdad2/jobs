const fs = require('fs');

class location {
	constructor(theFile) { 
		this.fileContent = theFile; 
		this.datas = {};
		this.objLocation = { Car: [], Rental: [] };	
		this.outputPath = '/jobs/backend/level1/data/output.json';
	}

	init() {
		this.readFile();
		this.setObjLocation();
		this.writeFile();
	}

	// Lecture du fichier Json
	readFile() {
		this.datas = fs.readFileSync(this.fileContent, 'utf8');	
	}

	// Parcours du fichier
	setObjLocation() {
		let obj = JSON.parse(this.datas);
		for (let i in obj.cars) {
			this.objLocation.Car.push(obj.cars[i]);
		}
		for (let i in obj.rentals) {
			this.objLocation.Rental.push(obj.rentals[i]);	
		}
	}

	// Calcul du prix de location par jour
	calc(car_id, start_date, end_date, distance) {
    	let price;
    	let carInfos = this.objLocation.Car;
		for (let i in carInfos) {
			if (carInfos[i].id == car_id) {
				let date1 = new Date(start_date);
				let date2 = new Date(end_date);
				let timeDiff = Math.abs(date2.getTime() - date1.getTime());
				let diffDays1 = Math.ceil((timeDiff + 1) / (1000 * 3600 * 24));
				return price = (diffDays1 * carInfos[i].price_per_day) + (distance * carInfos[i].price_per_km);
			}
	 	}
	}

	// Ecriture des résultats dans fichier JSON
	writeFile() {
		let allPrice;
		let objPrice = {
    		PriceRental: []
		};
		for (let i in this.objLocation.Rental) {
    		allPrice = this.calc(this.objLocation.Rental[i].car_id, this.objLocation.Rental[i].start_date, this.objLocation.Rental[i].end_date, this.objLocation.Rental[i].distance);
    		objPrice.PriceRental.push({
				id:this.objLocation.Rental[i].id, 
				price:allPrice
			});	
    	}
		let data = JSON.stringify(objPrice, null, 2);
		fs.writeFileSync(this.outputPath, data);
	}
}
module.exports = location;